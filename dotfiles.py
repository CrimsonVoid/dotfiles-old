#!/usr/bin/env python

from __future__ import print_function
from datetime import date
import hashlib
import time
import os, shutil
import errno

def mkdir(path):
    # os.makedirs(path, mode=0o755, exist_ok=True)
    try:
        os.makedirs(path)
    except OSError as err:
        if err.errno == errno.EEXIST or err.errno == errno.EISDIR:
            pass
        else:
            raise

def move(src, dst):
    try:
        shutil.move(src, dst)
        print('Moved {} to {}'.format(src, dst), end='')
    except (OSError, IOError) as err:
        if err.errno == errno.ENOENT:
            pass
        else:
            raise


def symlink(src, dst):
    try:
        os.symlink(src, dst)
        return True
    except OSError as err:
        if err.errno == errno.EEXIST or err.errno == errno.EISDIR:
            return False
        else:
            raise

def checksum(src, dst):
    src_md5 = dst_md5 = 0

    try:
        with open(src) as f:
            src_md5 = hashlib.md5(f.read()).hexdigest()
    except IOError as err:
        if err.errno == errno.EISDIR or err.errno == errno.ENOENT:
            return False
        else:
            raise
    try:
        with open(dst) as f:
            dst_md5 = hashlib.md5(f.read()).hexdigest()
    except IOError as err:
        if err.errno == errno.EISDIR or err.errno == errno.ENOENT:
            return False
        else:
            raise

    return src_md5 == dst_md5


if __name__ == '__main__':
    home_dir = os.path.expanduser('~')
    backup_dir = './old_dotfiles/{} - {}'.format(date.today(), time.strftime('%H.%M'))

    exclude = [os.path.basename(__file__), 'README.md', 'old_dotfiles']
    dotfiles = [d for d in os.listdir('.') if not d.startswith('.') and d not in exclude]

    mkdir(backup_dir)
    for d in dotfiles:
        symname = '{}/.{}'.format(home_dir, d)

        print('Linking {} to {}... '.format(d, symname), end='')
        if checksum(symname, d):
            print('Checksums match, skipping')
            continue

        move(symname, backup_dir)
        if symlink(os.path.abspath(d), symname) == True:
            print('\tOkay')
        else:
            print('\t{} exists. Continuing anyway'.format(symname))
