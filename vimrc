" Vundle
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" Plugins - General
Plugin 'bling/vim-airline'
Plugin 'gmarik/Vundle.vim'
Plugin 'godlygeek/tabular'
Plugin 'kien/ctrlp.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'scrooloose/syntastic'
Plugin 'SirVer/ultisnips'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-sensible'
Plugin 'Valloric/YouCompleteMe'

" Plugins - Language
Plugin 'fatih/vim-go'
Plugin 'rust-lang/rust.vim'
Plugin 'cespare/vim-toml'

" Plugins - Colors
Plugin 'w0ng/vim-hybrid'

call vundle#end()

" Vim Settings
syntax on
set t_Co=256
set colorcolumn=80,100
set laststatus=2
set noshowmode
set encoding=utf-8
set number
set ignorecase
set fillchars=vert:\│
set tabstop=4 expandtab shiftwidth=4 softtabstop=4
filetype plugin indent on
colorscheme hybrid

" Persistent views
autocmd BufWinLeave,BufWrite *.* mkview
autocmd BufWinEnter,BufRead  *.* silent loadview

" File extensions
autocmd BufRead,BufNewFile *.sls setfiletype yaml
autocmd BufRead,BufNewFile *.rs set colorcolumn=99

" Airline
let g:airline_powerline_fonts = 1

" YCM - Ultisnips
let g:UltiSnipsExpandTrigger = "<c-e>"
let g:UltiSnipsJumpForwardTrigger = "<c-e><c-n>"
let g:UltiSnipsJumpBackwardTrigger = "<c-e><c-p>"
